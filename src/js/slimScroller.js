var slimScroller = function (){
  'use strict';

  var elapsed, horizontal;
  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame;

  var scroll = function (target, horizontal, scrollTarget, durationTime, callbackFunction) {
      var scrollTargetReference = (scrollTarget && scrollTarget instanceof Element && scrollTarget) || document.documentElement;
      var duration = durationTime || 500;
      var horizontal = horizontal || false;
      var callback = callbackFunction || false;
      var startPosition = (horizontal) ? scrollTargetReference.scrollLeft : scrollTargetReference.scrollTop;
      var total = (horizontal) ? scrollTargetReference.scrollWidth : scrollTargetReference.scrollHeight;
      var targetPosition = Math.min(total, parsePosition(target, horizontal));
      var clock = Date.now();
      step(duration, horizontal, callback, startPosition, total, targetPosition, scrollTarget, clock)();
  };
  var easeInOutCubic = function (time) {
      return (time < 0.5) ? 4 * time * time * time : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1;
  };
  var calculatePosition = function (elapsed, duration, targetPosition, startPosition) {
      return (elapsed > duration) ? targetPosition : startPosition + (targetPosition - startPosition) * easeInOutCubic(elapsed / duration);
  };
  var parsePosition = function (target, horizontal){
      var parseNumber = parseInt(target);
      var parseElement = target.offsetLeft;
      var parseSelector;
      try{
          parseSelector = document.querySelector(target);
      }catch(e){}

      if (parseNumber) {
          // Target is pixel value
          return parseNumber;
      }else if(parseSelector){
          // Target is CSS-selector
          return (horizontal) ? document.querySelector(target).offsetLeft : document.querySelector(target).offsetTop;
      }else if(parseElement !== undefined){
          // Target is HTML-element
          return (horizontal) ? parseElement : target.offsetTop;
      }else{
          // Unknown type
          throw new Error('Unknown type as target');
      }
  };
  var step = function (duration, horizontal, callback, startPosition, total, targetPosition, scrollTarget, clock) {
      return function() {
          elapsed = Date.now() - clock;
          var stepPosition = calculatePosition(elapsed, duration, targetPosition, startPosition);
          var scrollTargetReference = (scrollTarget && scrollTarget instanceof Element && scrollTarget) || document.documentElement;
          if (horizontal){
              scrollTargetReference.scrollLeft = stepPosition;
          }else{
              scrollTargetReference.scrollTop = stepPosition;
          }
          if (elapsed > duration) {
              if (callback) {
                  callback([scrollTargetReference.scrollLeft,scrollTargetReference.scrollTop]);
              }
          } else {
              requestAnimationFrame(step(duration, horizontal, callback, startPosition, total, targetPosition, scrollTarget, clock));
          }
      }
  };
  return {
      scroll: scroll
  };
}();
