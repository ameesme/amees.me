window.addEventListener("load", function () {
  const mainView = document.querySelector("body .main");
  const mainViewContainer = document.querySelector("body .main .container");
  const mainViewInner = document.querySelector("body .main .container .inner");
  const sidebar = document.querySelector("body .sidebar");
  const promoSection = document.querySelector("section#promo");

  function hideFaderArrow() {
    mainViewContainer.removeEventListener("scroll", hideFaderArrow);
    document.querySelector(".fader-arrow").classList.add("hide");
  }

  // Optimizes for mobile-devices where multi-pane behaves badly
  function optimizeLayout() {
    if (window.innerWidth <= 768) {
      const mainViewHeight = mainViewInner.offsetHeight;
      mainView.style.height = mainViewHeight + "px";

      const newViewportHeight = `${window.innerHeight}px`;
      sidebar.style.height = newViewportHeight;
    }
  }

  function fadeInPhoto() {
    const backgroundPhotoNode = document.querySelector("#background-photo");
    setTimeout(() => {
      backgroundPhotoNode.classList.add("active");
    }, 500);
  }

  // Hide promo to show after a delay
  // function initializePromo() {
  //   const initialHeight = promoSection.offsetHeight;
  //   promoSection.classList.remove('show');

  //   // Show promo after three seconds
  //   setTimeout(function () {
  //     promoSection.style.height = initialHeight + 'px';
  //     promoSection.classList.add('show');
  //   }, 3000);
  // }

  function initializeDarkTheme() {
    if (window.matchMedia) {
      if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
        document.body.classList.add("theme-dark");
      }

      window
        .matchMedia("(prefers-color-scheme: dark)")
        .addEventListener("change", (e) => {
          if (e.matches) {
            document.body.classList.add("theme-dark");
          } else {
            document.body.classList.remove("theme-dark");
          }
        });
    }
  }

  function initializeReveal() {
    const revealParagraph = document.querySelector("p.reveal");
    const spans = revealParagraph.querySelectorAll("span");
    spans.forEach((item) => {
      item.classList.add("hidden");
    });
  }

  mainViewContainer.addEventListener("scroll", hideFaderArrow);
  window.addEventListener("resize", optimizeLayout);
  optimizeLayout();
  fadeInPhoto();
  initializeDarkTheme();
  initializeReveal();
  // initializePromo();
});

function scrollContainer(selector) {
  const target = (selector && document.querySelector(selector)) || 600;
  slimScroller.scroll(target, false, document.querySelector(".container"));
}

function scrollSidebar(selector) {
  slimScroller.scroll(
    document.querySelector(selector),
    true,
    document.querySelector(".sidebar")
  );
}

function scrollWindow(selector, callback) {
  slimScroller.scroll(
    document.querySelector(selector),
    false,
    null,
    null,
    callback
  );
}

function openModal(selector, event) {
  event.preventDefault();
  event.stopPropagation();
  const selectedModalNode = document.querySelector(selector);
  const pageContainerNode = document.querySelector(".pages");
  const sidebarNode = document.querySelector(".sidebar");

  document.body.classList.add("modal-open");
  slimScroller.scroll(selectedModalNode, true, sidebarNode);
  scrollWindow(".sidebar");
}

function closeModal() {
  scrollWindow(".main section#jobs", function () {
    slimScroller.scroll(
      document.querySelector("#start"),
      true,
      document.querySelector(".sidebar"),
      1
    );
    document.body.classList.remove("modal-open");
  });
}

function closePromo() {
  const promoSection = document.querySelector("section#promo");
  promoSection.classList.remove("show");
  promoSection.style.height = 0;
}

function reveal(event, id) {
  const revealSpan = document.querySelectorAll("span." + id);
  revealSpan.forEach((item) => {
    item.classList.remove("hidden");
  });
  event.target.classList.add("clicked");

  const clickedAll =
    document.querySelectorAll(".reveal a:not(.clicked)").length === 0;
  if (clickedAll) {
    document.querySelector("p.reveal-end").classList.add("show");
  }
}
