// Dependencies
const express = require("express");
const bodyParser = require("body-parser");
const harp = require("harp");

// Modules
const mail = require(`${__dirname}/modules/mail`);

// Instances
const application = express();

// Middleware
application.use(bodyParser.json());

// Mail API

application.use(harp.mount(`${__dirname}/src`));

module.exports = application;
application.listen(8080, () => {
  console.log("Server started");
});
