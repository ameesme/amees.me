// Dependencies
const sendgridMail = require('@sendgrid/mail');
const Nedb = require('nedb');
const crypto = require('crypto');

// Instances
const db = new Nedb({ filename: './mailDatabase.db', autoload: true });
sendgridMail.setApiKey(process.env.SENDGRID_API_KEY);

// Methods
function scheduleMail (emailAddress, request) {
  const hashedEmail = crypto.createHash('sha256').update(emailAddress, 'ascii').digest('hex');
  const emailDoc = {
    email: hashedEmail,
    timestamp: new Date(),
    ip: request.ip,
    ua: request.headers['user-agent']
  };
  
  // Message and delay to pretend like I'm actually sending these manually
  // Welcome to the kingdom of bull****
  const emailText = 'Hi!\r\n\r\n' +
    'Hope you\'re doing well. I noticed you entered your email address at my website and thought I\'d get back to you right away.\r\n' +
    'Is there anything in particular you\'d like to discuss?\r\n\r\n' +
    'Thanks!\r\n\r\n' + 
    'Cheers,\r\n' +
    'Mees Boeijen\r\nmees@amees.me';
  const emailDelay = (Math.floor(Math.random() * 30) + 5) * 60 * 1000;
  const message = {
    to: [emailAddress, 'mees@amees.me'],
    from: 'mees@amees.me',
    subject: 'Your contact request on my website',
    text: emailText
  };
    
  setTimeout(() => {
    // Check if email is already in database
    db.find({ email: hashedEmail }, function (err, docs) {
      if (err) {
        console.log(`[DB / ERROR] ${error.toString()}`);
      }
      if (docs && docs.length > 0) {
        return;
      }
      db.insert(emailDoc, (error, newDoc) => {
        if (error) {
          console.log(`[DB / ERROR] ${error.toString()}`);
        }
        sendgridMail.send(message)
          .then(() => {
            console.log(`[MAIL] Dispatched mail for ${emailAddress}`);
          })
          .catch((error) => {
            console.log(`[MAIL / ERROR] ${error.toString()}`);
          })
      });
    });
  }, emailDelay);
  console.log(`[MAIL] Scheduled mail for ${emailAddress} to be dispatched in ${emailDelay/1000} seconds`);
}

// Export
exports.handleMailRequest = (request, response) => {
  // Check if request is complete
  const requestedEmail = (request.body && request.body.email);
  if (!requestedEmail) {
    response.status(400);
    return response.end('I need your email address to contact you.');
  }

  // Check if email is valid
  const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
  if (!emailRegex.test(requestedEmail)) {
    response.status(400);
    return response.end('This email address seems incorrect.')
  }

  // Hash email
  const hashedEmail = crypto.createHash('sha256').update(requestedEmail, 'ascii').digest('hex');

  // Check if email is already in database
  db.find({ email: hashedEmail }, function (err, docs) {
    if (err) {
      console.log(`[DB / ERROR] ${error.toString()}`);
      response.status(500);
      return response.end('Something went wrong. Please try that again.')
    }
    if (docs && docs.length > 0) {
      response.status(429);
      return response.end('I already sent you an email :).')
    }
    scheduleMail(requestedEmail, request);
    response.status(200);
    return response.end('Thanks! I\'ll contact you soon :).');
  });
}