FROM node:9.5.0

WORKDIR /usr/src/app
COPY package.json ./
COPY yarn.lock ./
COPY src/ ./src/
COPY modules/ ./modules/
COPY index.js ./index.js

RUN yarn

EXPOSE 8080

CMD node /usr/src/app/index.js